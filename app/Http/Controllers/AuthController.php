<?php
namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request) {
        try {
            $fields = $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|max:255|unique:users,email',
                'address'=>'required|string',
                'phone'=> 'required|string',
                'password' => 'required|string|min:6|confirmed'
            ]);

            $user = User::create([
                'name' => $fields['name'],
                'email' => $fields['email'],
                'address' => $fields['address'],
                'phone' => $fields['phone'],
                'password' => bcrypt($fields['password'])
            ]);

            $token = $user->createToken('happen')->plainTextToken;

            return response()->json([
                'status_code' => 200,
                'user' => $user,
                'token' => $token,
                'token_type' => 'Bearer',
                'message' => 'Registration Successful'
            ]);

        } catch(Exception $error) {
            return response()->json([
                'status_code' => 500,
                'message' => 'Error in Registration',
                'error' => $error,
            ]);
        }
    }

    public function login(Request $request) {
        try {
            $fields = $request->validate([
                'email' => 'required|string',
                'password' => 'required|string'
            ]);

            // Check email
            $user = User::where('email', $fields['email'])->first();

            // Check password
            if(!$user || !Hash::check($fields['password'], $user->password)) {
                return response([
                    'message' => 'Bad creds'
                ], 401);
            }

            $token = $user->createToken('happen')->plainTextToken;

            return response()->json([
                'status_code' => 200,
                'user' => $user,
                'token' => $token,
                'token_type' => 'Bearer',
                'message' => 'You are lodged in Successful'
            ]);
        } catch(Exception $error) {
            return response()->json([
                'status_code' => 500,
                'message' => 'Error in login',
                'error' => $error,
            ]);
        }
    }

    public function logout(Request $request) {
        auth()->user()->tokens()->delete();
        $response = [
            'message' => 'Logged out'
        ];
        return response($response, 200);
    }
}
